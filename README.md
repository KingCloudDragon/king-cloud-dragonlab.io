# Swagger2注解生成API说明

> 建议自动[生成实体，点我查看如何生成](./数据库/数据库操作相关说明.html)

> 【前提】项目开启Swagger2支持
- pom引用
```
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
</dependency>
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
</dependency>
```
- 代码配置
```java
@Configuration
@EnableSwagger2
@SuppressWarnings("SpringFacetCodeInspection")
public class Swagger2Config {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo()).select()
                .apis(RequestHandlerSelectors.basePackage("org.dppc.trace.base.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("ipt-trace-base:追溯管理系统")
                .description("追溯管理系统API")
                .version("1.0")
                .build();
    }

}
```

> 文档访问地址：[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

## **一、相关注解**

1. **`ApiOperation`**: 标明这个请求是做什么的，使用位置:`方法上`
```
@ApiOperation(value="获取商户分页数据", notes="获取商户分页数据")
```
2. **`RequestMapping`**: 最好加上method说明请求是何种方式，默认为全部方式
```
@RequestMapping(value = "/findBizPage", method = {RequestMethod.GET, RequestMethod.POST})
```
3. **`ModelAttribute`**: 大多数情况下的请求方式(普通情况下的get参数拼接、表单提交)，
使用对象接受参数，需要配合*`ApiModelProperty`*一起使用，使用位置:`请求参数`
```
@ModelAttribute BusinessBaseInfo query
```
4. **`RequestParam`**: 大多数情况下的请求方式(普通情况下的get参数拼接、表单提交)，请求参数直接作为方法参数，
需要配合*`ApiParam`*一起使用，使用位置:`请求参数`
```
@RequestParam(defaultValue = "1") @ApiParam(value = "当前页数") Integer page
```
5. **`RequestBody`**: 大多数情况下的请求方式(JSON格式的请求)，使用对象接受参数，
需要配合*`ApiImplicitParam`*一起使用，使用位置:`请求参数`
```
@RequestParam(defaultValue = "1")
```
6. **`ApiModelProperty`**: 标明对象字段的含义，使用位置:`对象的属性字段上`
```
@ApiModelProperty(value = "主键")
private Long Id;
```
7. **`ApiParam`**: 标明API请求参数的含义，使用位置:`对象的属性字段上`
```
@ApiParam(value = "当前页数") Integer page
```
8. **`ApiImplicitParam`**: 标明API请求参数(JSON格式的，数据在请求体内的)的含义，使用位置:`方法上`
```
@ApiImplicitParam(value = "商户对象", dataType = "BusinessBaseInfo.class")
```
9. **`ApiImplicitParams`**: 当有多个*`ApiImplicitParam`*可以使用这个，使用位置:`方法上`
```
@ApiImplicitParams(value = {
    @ApiImplicitParam(value = "商户对象", dataType = "BusinessBaseInfo.class")
    // , @ApiImplicitParam(value = "others", dataType = "Others.class")
    // ...
})
```
10. **`PathVariable`**: 当参数是从*`RequestMapping`*的请求地址上传过来的时候，
需要配合*`ApiParam`*一起使用，使用位置:`参数上`
```
@RequestMapping(value = "/{id}", method = {RequestMethod.GET})
public BaseResponse findOne(@PathVariable @ApiParam(value = "商户ID") Long id) { }
```

## **二、相关举例**

1. 只有一段
```java
@RestController
@RequestMapping(value="/biz")
public class BusinessInfoController {

    @Autowired
    private BusinessBaseInfoService businessBaseInfoService;

    // [ApiOperation]这个请求是做什么的
    @ApiOperation(value="获取商户分页数据", notes="获取商户分页数据")
    // 使用RequestMapping的method指定查询方式
    @RequestMapping(value = "/findBizPage", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public BaseResponse findBizPage(
            // [ModelAttribute + ApiModelProperty]用对象接受数据，对象的字段注解往下看
            @ModelAttribute BusinessBaseInfo query
            // [RequestParam + ApiParam]当前页数
            , @RequestParam(defaultValue = "1") @ApiParam(value = "当前页数") Integer page
            // [RequestParam + ApiParam]分页大小
            , @RequestParam(defaultValue = "5") @ApiParam(value = "分页大小") Integer size) {
        return ResponseHelper.success(businessBaseInfoService
                .findBusinessInfoPage(query, new PageRequest(page - 1, size)).getContent());
    }

    // [ApiOperation]这个请求是做什么的
    @ApiOperation(value="查看商户信息", notes="查看商户信息")
    // 请求参数从路径穿过来
    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    @ResponseBody
    public BaseResponse findOne(
            // [PathVariable + ApiParam]说明参数含义
            @PathVariable @ApiParam(value = "商户ID") Long id) {
        return ResponseHelper.success(businessBaseInfoService.getOneDataById(id));
    }

}

// JPA 注解
public class BusinessBaseInfo implements Serializable {

    // JPA 注解
    @ApiModelProperty(value = "主键")
    private Long businessBaseInfoId;
    // JPA 注解
    @ApiModelProperty(value = "经营者编码")
    private String bizCode;

    // getter & setter

}
```